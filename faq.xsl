<?xml version="1.0"?>
<xsl:stylesheet  
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:fo="http://www.w3.org/1999/XSL/Format"
 version="1.0">

  <xsl:import href="xhtml/chunk.xsl"/>

  <xsl:param name="html.stylesheet" select="'faq.css'"/> 
  <xsl:param name="section.autolabel" select="1"/>
  <xsl:param name="qandadiv.autolabel" select="1"/>
  <xsl:param name="use.id.as.filename" select="1"/>
  <xsl:param name="shade.verbatim" select="1"/>
  <xsl:param name="css.decoration" select="1"/>
  <xsl:param name="make.valid.html" select="1"/>
  <xsl:param name="html.extra.head.links" select="1"/>
  <xsl:param name="chunk.first.sections" select="1"/>
  <xsl:param name="chunker.output.encoding" select="'ISO-8859-1'"/>
  
  <xsl:template name="user.head.content">
    <xsl:comment> generated HTML; do not edit </xsl:comment>
  </xsl:template>

</xsl:stylesheet> 
