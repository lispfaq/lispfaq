<!DOCTYPE style-sheet PUBLIC "-//James Clark//DTD DSSSL Style Sheet//EN" [
<!ENTITY % html "IGNORE">
<![%html;[
<!ENTITY % print "IGNORE">
<!ENTITY docbook.dsl 
PUBLIC "-//Norman Walsh//DOCUMENT DocBook HTML Stylesheet//EN"
--SYSTEM "docbook.dsl"-- 
CDATA dsssl>
]]>
<!ENTITY % print "INCLUDE">
<![%print;[
<!ENTITY docbook.dsl 
PUBLIC "-//Norman Walsh//DOCUMENT DocBook Print Stylesheet//EN"
-- SYSTEM "docbook.dsl" -- 
CDATA dsssl>
]]>
]>

<style-sheet>

<style-specification id="print" use="docbook">
<style-specification-body> 

;; ==============================
;; customize the print stylesheet
;; ==============================

(declare-characteristic preserve-sdata?
  ;; this is necessary because right now jadetex does not understand
  ;; symbolic entities, whereas things work well with numeric entities.
  "UNREGISTERED::James Clark//Characteristic::preserve-sdata?"
  #f)

(define %generate-article-toc%
  ;; Should a Table of Contents be produced for Articles?
  #t)

(define (toc-depth nd)
  2)

(define %generate-article-titlepage-on-separate-page%
  ;; Should the article title page be on a separate page?
  #t)

(define %section-autolabel%
  ;; Are sections enumerated?
  #t)

(define %footnote-ulinks%
  ;; Generate footnotes for ULinks?
  #f)

(define %bop-footnotes%
  ;; Make "bottom-of-page" footnotes?
  #f)

(define %body-start-indent%
  ;; Default indent of body text
  0pi)

(define %para-indent-firstpara%
  ;; First line start-indent for the first paragraph
  0pt)

(define %para-indent%
  ;; First line start-indent for paragraphs (other than the first)
  0pt)

(define %block-start-indent%
  ;; Extra start-indent for block-elements
  0pt)

(define formal-object-float
  ;; Do formal objects float?
  #t)

(define %hyphenation%
  ;; Allow automatic hyphenation?
  #t)

(define %admon-graphics%
  ;; Use graphics in admonitions?
  #f)

(define %paper-type% "A4")

</style-specification-body>
</style-specification>


<!--
;; ===================================================
;; customize the html stylesheet; borrowed from Cygnus
;; at http://sourceware.cygnus.com/ (cygnus-both.dsl)
;; ===================================================
-->

<style-specification id="html" use="docbook">
<style-specification-body> 

(declare-characteristic preserve-sdata?
  ;; this is necessary because right now jadetex does not understand
  ;; symbolic entities, whereas things work well with numeric entities.
  "UNREGISTERED::James Clark//Characteristic::preserve-sdata?"
  #f)

(define %generate-legalnotice-link%
  ;; put the legal notice in a separate file
  #t)

(define %admon-graphics-path%
  ;; use graphics in admonitions, set their
  "../images/")

(define %admon-graphics%
  #t)

(define %funcsynopsis-decoration%
  ;; make funcsynopsis look pretty
  #t)

(define %html-ext%
  ".html")

(define %generate-article-toc% 
  ;; Should a Table of Contents be produced for Articles?
  ;; If true, a Table of Contents will be generated for each 'Article'.
  #t)

(define %generate-part-toc% #t)

(define %generate-article-titlepage% #t)

(define ($generate-qandaset-toc$) #t)

(define %footer-navigation% #t)

(define %qanda-inherit-numeration% #t)

(define nochunks #f)

(define %stylesheet% "lisp-faq.css")

(define %css-decoration% #t)

;(define (chunk-skip-first-element-list)
;  ;; forces the Table of Contents on separate page
;  '())

(define %root-filename%
  ;; The filename of the root HTML document (e.g, "index").
  "lispfaq")

(define %shade-verbatim% #t)

(define %use-id-as-filename%
  ;; Use ID attributes as name for component HTML files?
  #t)

(define %graphic-default-extension% 
  "gif")

(define %section-autolabel%
  ;; For enumerated sections (1.1, 1.1.1, 1.2, etc.)
  #t)

;(define (toc-depth nd)
;  ;; more depth, 2 levels, to toc, instead of flat hierarchy
;  5)

(define biblio-citation-check #t)

(define biblio-number #t)


(define (chunk-element-list)
  (list (normalize "qandaset")
        (normalize "qandadiv")
        (normalize "preface")
	(normalize "chapter")
	(normalize "appendix") 
	(normalize "article")
	(normalize "glossary")
	(normalize "bibliography")
	(normalize "index")
	(normalize "colophon")
	(normalize "setindex")
	(normalize "reference")
	(normalize "refentry")
	(normalize "part")
	(normalize "sect1") 
	(normalize "section") 
	(normalize "book") ;; just in case nothing else matches...
	(normalize "set")  ;; sets are definitely chunks...
	(normalize "qandaset")
        (normalize "qandadiv")))

(define (chunk-skip-first-element-list)
  (list (normalize "sect1")
	(normalize "section")
        (normalize "qandaset")
        (normalize "qandadiv")))

(define (chunk-section-depth)
  5)



(define (chunk? #!optional (nd (current-node)))
  (let* ((notchunk (or (and (equal? (gi nd) (normalize "bibliography"))
			    (not (or (equal? (gi (parent nd)) (normalize "book"))
				     (equal? (gi (parent nd)) (normalize "article")))))
		       (and (equal? (gi nd) (normalize "section"))
			    (equal? (gi (parent nd)) (normalize "section"))
			    (>= (section-element-depth nd)
				(chunk-section-depth)))))
	 (maybechunk (not notchunk)))
    (if (node-list=? nd (sgml-root-element))
	#t
	(if (or nochunks
		(equal? (dbhtml-value (sgml-root-element) "chunk") "no"))
	    #f  ;; emarsden was #f
	    (if (member (gi nd) (chunk-element-list))
		(if (combined-chunk? nd)
		    #t  ;; emarsden was #f
		    maybechunk)
		#t)))))

(define (combined-chunk? #!optional (nd (current-node)))
  #f)


;(element (toc)
;   (build-toc (parent (current-node)) (toc-depth (parent (current-node)))))

</style-specification-body>
</style-specification>

<external-specification id="docbook" document="docbook.dsl">

</style-sheet>
